import CountDown from "./components/CountDown/CountDown";
import DigitalClock from "./components/DigitalClock/DigitalClock";
import StopWatch from "./components/StopWatch/StopWatch";
import { AiOutlineCopyrightCircle } from "react-icons/ai";
import "./App.css";

function App() {
  let year = new Date().getFullYear().toLocaleString();

  return (
    <div className="App">
      <header>My watch</header>
      <div className="app-content">
        <h1 className="app-quote">Time</h1>
        <p className="app-quotecontent">
          "has a wonderful way of <br /> showing us what really matters."
        </p>
      </div>
      <DigitalClock />
      <CountDown />
      <StopWatch />
      <footer>
        {" "}
        <AiOutlineCopyrightCircle /> Copyrigth {year}.{" "}
      </footer>
    </div>
  );
}

export default App;
