/*import React, { useState, useEffect, useContext } from "react";

export const Theme = React.createContext();

export const ThemeUpdated = React.createContext();

export const useTheme = () => {  
    
    return useContext(Theme);
    };
    
    export const useThemeUpdated = () => {  
        return useContext(ThemeUpdated);};
    export function ThemeProvider({ children }) {  
        const [darkMode, setDarkMode] = useState(false);  
        useEffect(() => {    
            document.body.style.backgroundColor = darkMode      ? "linear-gradient(-45deg, #3c096c, #5a189a, #7b2cbf, #ff006e)"      : "#140224";  }, [darkMode]);  
            const toggleTheme = () => {    
                setDarkMode((prevDarkMode) => !prevDarkMode);  };

return (    
    <Theme.Provider value={darkMode}>     
     <ThemeUpdated.Provider value={toggleTheme}>     
        {children}    
          </ThemeUpdated.Provider>
              </Theme.Provider>  );}/*** * 


import { ThemeProvider } from "./Theme/Theme";
import { useTheme, useThemeUpdated } from "../../Theme/Theme";
import { ImSun } from "react-icons/im";import { ImBrightnessContrast } from "react-icons/im";

const darkMode = useTheme();  
const toggleTheme = useThemeUpdated();  
const themeStyles = {    
    backgroundColor: darkMode  ? "#140224" : "linear-gradient(-45deg, #3c096c, #5a189a, #7b2cbf, #ff006e)", color: "#fff", }; */
