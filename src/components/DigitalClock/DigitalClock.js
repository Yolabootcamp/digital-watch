import React, { useState, useEffect } from "react";
import "./DigitalClock.css";
import { ImClock } from "react-icons/im";

export default function DigitalClock() {
  const [clockState, setclockState] = useState();

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setclockState(date.toLocaleTimeString());
    }, [1000]);
  });

  return (
    <div className="digitalclock-container">
      <div className="digital-clock">
        <ImClock className="digitalclock-icon" />
        <p className="digitalclock-name">Digital Clock</p>
        <h2 className="digital-counter">{clockState}</h2>
      </div>
    </div>
  );
}
