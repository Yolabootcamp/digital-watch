import React, { useState, useEffect } from "react";
import "./StopWatch.css";
import { ImStopwatch } from "react-icons/im";
import { ImPlay2 } from "react-icons/im";
import { ImStop } from "react-icons/im";
import { ImPause } from "react-icons/im";
import { ImLoop2 } from "react-icons/im";

const StopWatch = () => {
  const [time, setTime] = useState(0);
  const [timeOn, setTimeOn] = useState(false);
  useEffect(() => {
    let interval = null;
    if (timeOn) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 10);
      }, 10);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [timeOn]);

  return (
    <div className="stop-watch-container">
      <div className="stopwatch">
        <ImStopwatch className="stop-icons" />
        <p className="stopwatch-name">StopWatch</p>
        <h2>
          <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
          <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
          <span>{("0" + ((time / 10) % 100)).slice(-2)}</span>
        </h2>
        <div>
          <div id="buttons" className="buttons">
            {!timeOn && time === 0 && (
              <ImPlay2
                className="icons"
                onClick={() => {
                  setTimeOn(true);
                }}
              />
            )}
            {timeOn && (
              <ImStop
                className="icons"
                onClick={() => {
                  setTimeOn(false);
                }}
              />
            )}
            {!timeOn && time > 0 && (
              <ImLoop2 className="icons" onClick={() => setTime(0)} />
            )}
            {!timeOn && time > 0 && (
              <ImPause
                className="icons"
                onClick={() => {
                  setTimeOn(true);
                }}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default StopWatch;
