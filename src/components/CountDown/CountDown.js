import React, { useState, useEffect } from "react";
import "./CountDown.css";
import { ImHistory } from "react-icons/im";

const CountDown = () => {
  const [time, setTime] = useState("");

  useEffect(() => {
    let countDownDate = new Date("Octuber 31, 2021 18:30:08").getTime();
    let x = setInterval(() => {
      let now = new Date().getTime();
      let distance = countDownDate - now;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      setTime(days + " d " + hours + " h " + minutes + " m " + seconds + " s ");
      if (distance < 0) {
        clearInterval(x);
        setTime("COUNTDOWN FINISHED");
      }
    }, 1000);
  }, []);

  return (
    <div className="countdown-container">
      <div className="count-down">
        <ImHistory className="countdown-icons" />
        <div>
          <p className="countdown-name">CountDown Timer</p>
          <p className="countdown-time">{time}</p>
        </div>
      </div>
    </div>
  );
};
export default CountDown;
